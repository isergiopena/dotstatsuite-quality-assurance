# security dashboard
*All links only accessible to team members*

## definitions
**DAST:** Dynamic Application Security Testing  
**SAST:** Static Application Security Testing  
**SCA:** Software Composition Analysis, aka Dependency Scanning

## services

<table>
<thead>
<tr><th>service/repo</th><th>scan type</th><th>tool</th><th>setup</th><th>status</th></tr>
</thead>
<body>
<tr>
<td rowspan="7">[data-explorer](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-data-explorer)</td>
<td rowspan="3">DAST</td>
<td>[GitLab](https://gitlab.com/-/security/vulnerabilities/?reportType=DAST&projectId=10532325)</td>
<td></td>
<td>pending [MR](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-quality-assurance/-/merge_requests/38/diffs)</td>
<tr>
<td>[Veracode](https://web.analysiscenter.veracode.com/was/#/analysis/96f54737d3869e4adf8713c76911846e/scans)</td>
<td>:timer: scheduled</td>
<td>-</td>
<tr>
<td>[Invicti](https://www.netsparkercloud.com/websites/dashboard/81beee08248248adb304ac5502bb4340/)</td>
<td>:timer: scheduled <br> :wrench: [quality-assurance-ci.yml](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-quality-assurance/-/blob/master/.netsparker-security-test-gitlab-ci.yml) & ?</td>
<td>-</td>
</tr>
<td rowspan="2">SAST</td>
<td>[GitLab](https://gitlab.com/-/security/vulnerabilities/?reportType=SAST&projectId=10532325)</td>
<td>every commit/merge to dev</td>
<td>pending [MR](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-quality-assurance/-/merge_requests/38/diffs)</td>
<tr>
<td>[Veracode](https://analysiscenter.veracode.com/auth/index.jsp#HomeAppProfile:90000:1365574)</td>
<td></td>
<td>-</td>
</tr>
<td rowspan="2">SCA</td>
<td>[GitLab](https://gitlab.com/-/security/vulnerabilities/?reportType=DEPENDENCY_SCANNING&projectId=10532325)</td>
<td>every commit/merge to dev</td>
<td>-</td>
<tr>
<td>[Veracode](https://analysiscenter.veracode.com/auth/index.jsp#ReviewResultsSCA:90000:1365574:17037123::17025119::::::)</td>
<td></td>
<td>-</td>
</tr>
<tr>
<td rowspan="7">[sdmx-faceted-search](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-sdmx-faceted-search)</td>
<td rowspan="3">DAST</td>
<td>[GitLab](https://gitlab.com/-/security/vulnerabilities/?reportType=DAST&projectId=10283564)</td>
<td></td>
<td>pending [MR](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-quality-assurance/-/merge_requests/38/diffs)</td>
<tr>
<td>[Veracode]()</td>
<td></td>
<td> pending [OpenAPI 2.0/3.0 schema](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-sdmx-faceted-search/-/issues/119)</td>
<tr>
<td>[Invicti](https://www.netsparkercloud.com/websites/dashboard/304d75aaeb0c4bc489f2acf0032528ec/)</td>
<td>:timer: scheduled <br> :wrench: [quality-assurance-ci.yml](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-quality-assurance/-/blob/master/.netsparker-security-test-gitlab-ci.yml) & ?</td>
<td>-</td>
</tr>
<td rowspan="2">SAST</td>
<td>[GitLab](https://gitlab.com/-/security/vulnerabilities/?reportType=SAST&projectId=10283564)</td>
<td></td>
<td>pending [MR](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-quality-assurance/-/merge_requests/38/diffs)</td>
<tr>
<td>[Veracode]()</td>
<td></td>
<td> to be setup</td>
</tr>
<td rowspan="2">SCA</td>
<td>[GitLab](https://gitlab.com/-/security/vulnerabilities/?reportType=DEPENDENCY_SCANNING&projectId=10283564)</td>
<td>every commit/merge to dev</td>
<td>-</td>
<tr>
<td>[Veracode](https://analysiscenter.veracode.com/auth/index.jsp#ReviewResultsSCA:90000:1365574:17037123::17025119::::::)</td>
<td></td>
<td>-</td>
</tr>
<tr>
<td rowspan="7">[core-transfer](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer)</td>
<td rowspan="3">DAST</td>
<td>[GitLab](https://gitlab.com/-/security/vulnerabilities/?reportType=DAST&projectId=11609192)</td>
<td>:wrench: [quality-assurance-ci.yml](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-quality-assurance/-/blob/master/.netsparker-security-test-gitlab-ci.yml) & [transfer-ci.yml](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/blob/develop/.gitlab-ci.yml)</td>
<td>-</td>
<tr>
<td>[Veracode](https://web.analysiscenter.veracode.com/was/#/analysis/4e569e2b39d31d7cbc56cf4e41919297/scans)</td>
<td></td>
<td>-</td>
<tr>
<td>[Invicti](https://www.netsparkercloud.com/websites/dashboard/5b325106be0f4cc088fcac5502aeda1c/)</td>
<td>:timer: scheduled <br> :wrench: [quality-assurance-ci.yml](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-quality-assurance/-/blob/master/.netsparker-security-test-gitlab-ci.yml) & ?</td>
<td>-</td>
</tr>
<td rowspan="2">SAST</td>
<td>[GitLab](https://gitlab.com/-/security/vulnerabilities/?reportType=SAST&projectId=11609192)</td>
<td>every commit/merge to dev</td>
<td>-</td>
<tr>
<td>[Veracode](https://analysiscenter.veracode.com/auth/index.jsp#HomeAppProfile:90000:1365641)</td>
<td></td>
<td>-</td>
</tr>
<td rowspan="2">SCA</td>
<td>[GitLab](https://gitlab.com/-/security/vulnerabilities/?reportType=DEPENDENCY_SCANNING&projectId=11609192)</td>
<td>every commit/merge to dev</td>
<td>-</td>
<tr>
<td>[Veracode](https://analysiscenter.veracode.com/auth/index.jsp#ReviewResultsSCA:90000:1365641:17077651::17065639::::::)</td>
<td></td>
<td>-</td>
</tr>
</body>
</table>

