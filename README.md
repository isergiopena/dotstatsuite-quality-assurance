
Contents
---------

[[_TOC_]]

----------
# .Stat Suite Core: performance results

| .Stat Suite .NET | [v6.0.0](https://gitlab.com/groups/sis-cc/.stat-suite/-/milestones/34) | [v6.1.0](https://gitlab.com/groups/sis-cc/.stat-suite/-/milestones/37) | [v6.4.0](https://gitlab.com/groups/sis-cc/.stat-suite/-/milestones/41) | [v8.0.1](https://gitlab.com/groups/sis-cc/.stat-suite/-/milestones/51) | [v8.1.2](https://gitlab.com/groups/sis-cc/.stat-suite/-/milestones/53) | [almond](https://gitlab.com/groups/sis-cc/.stat-suite/-/milestones/55) | [blueberry](https://gitlab.com/groups/sis-cc/.stat-suite/-/milestones/57) | [dragonfruit](https://gitlab.com/groups/sis-cc/.stat-suite/-/milestones/59) | [elote](https://gitlab.com/groups/sis-cc/.stat-suite/-/milestones/70) | Trends (av. minus last release) |
|---------------------------------|-------------------|-------------------|-----------------------|-----------------------|-----------------------|-----------------------|---------------------|---------------------|---------------------|---------------------|
| Release date | Mar-21 | Apr-21 | May-21 | Mar-22 | May-22 | Aug-22 | Nov-22 | Jul-23 | Aug-24 | - |
| nsiws | - | - | v8.2.0 | v8.9.2 | v8.9.2 | v8.9.2 | v8.12.2 | v8.17.0 | v8.18.2 | - |
| transfer | v6.0.0 | v6.1.0 | - | v8.0.1 - v9.0.1 | v8.1.2 - v9.1.2 | - | v11.0.7 | v12.0.1 | **v13.0.0** | - |
| | | | | | | | | | | |
| **Smoke-test data imports** | 85.71% ✓ 6 ✗ 1 | 85.71% ✓ 6 ✗ 1 | - | 71.42% ✓ 5 ✗ 2 | 100.00% ✓ 13 ✗ 0 | - | 100.00% ✓ 13 ✗ 0 | 100.00% ✓ 13 ✗ 0 | 100.00% ✓ 13 ✗ 0 | - |
| - data_import_time | 13.13s | 10.14s | - | 12.68s | 24.21s | - | 20.73s | 13.31s | **15.72s** | *15.7s* |
| - datasetSize:extraSmall | 2.42s | 6.13s | - | 2.19s | 2.37s | - | 1.5s | 1.14s | **1.02s** | *2.62s* |
| - datasetSize:small | 23.85s | 18.17s | - | 28.43s | 25.48s | - | 27.84s | 16.63s | **18.55** | *23.4s* |
| - datasetSize:large | - | - | - | 38.14s | - | - | 29.99s | 19.76s | **24.1s** | *29.29s* |
| **Smoke-test data extractions** | - | - | 99.41% ✓ 342 ✗ 2 | 100.00% ✓ 160 ✗ 0 | 100.00% ✓ 344 ✗ 0 | 100.00% ✓ 344 ✗ 0 | 100.00% ✓ 344 ✗ 0 | 100.00% ✓ 344 ✗ 0 | 100.00% ✓ 344 ✗ 0 | - |
| - http_req_duration | - | - | 311.59ms | 304.54ms | 269.97ms | 304.54ms | 400.56ms | 221.76ms | **240.76ms** | *302.16ms* |
| - datasetSize:extraSmall | - | - | 222.77ms | 260.97ms | 219.58ms | 260.97ms | 208.65ms | 179.3ms | **166.9ms** | *225.37ms* |
| - datasetSize:small | - | - | 238.1ms | 244.05ms | 209.68ms | 244.05ms | 224.02ms | 171.31ms | **169.19ms** | *221.86ms* |
| **Load-test data extractions** | - | - | 93.58% ✓ 9971 ✗ 684 | 96.97% ✓ 11398 ✗ 356 | 92.81% ✓ 9303 ✗ 720 | 96.97% ✓ 11398 ✗ 356 | 99.90% ✓ 10256 ✗ 10 | 100.00% ✓ 15678 ✗ 0 | 100.00% ✓ 12838 ✗ 0 | - |
| - http_req_duration | - | - | 2.46s | 2.12s | 2.67s | 2.12s | 2.58s | 1.33s | **1.85s** | *2.21s* |
| - datasetSize:extraSmall | - | - | 2.21s | 2.40s | 2.71s | 2.4s | 2.17s | 1.2s | **1.65s** | *2.18s* |
| - datasetSize:small | - | - | 2.19s | 1.93s | 2.41s | 1.93s | 2.12s | 1.13s | **1.54s** | *1.95s* |
| **Stress-test data extractions** | - | - | 95.67% ✓ 8516 ✗ 385 | 85.74% ✓ 10428 ✗ 1734 | 97.91% ✓ 9464 ✗ 202 | 85.74% ✓ 10428 ✗ 1734 | 100.00% ✓ 10249 ✗ 0 | 100.00% ✓ 15613 ✗ 0 | 100.00% ✓ 11468 ✗ 0 | - |
| - http_req_duration | - | - | 4.15s | 2.78s | 3.74s | 2.78s | 3.47s | 1.93s | **2.99s** | *3.14s* |
| - datasetSize:extraSmall | - | - | 3.51s | 2.67s | 3.65s | 2.67s | 2.87s | 1.77s | **2.61s** | *2.85s* |
| - datasetSize:small | - | - | 3.91s | 2.47s | 3.5s | 2.47s | 3.02s | 1.69s | **2.62s** | *2.84s* |
| - datasetSize:small_paginated | - | - | 3.47s | 1.63s | 2.74s | 1.63s | 3.17s | 1.76s | **2.72s** | *2.4s* |
| **Spike-test data extractions** | - | - | 72.12% ✓ 2921 ✗ 1129 | 70.74% ✓ 3516 ✗ 1454 | 68.14% ✓ 3480  ✗ 1627 | 70.74% ✓ 3516 ✗ 1454 | 99.01% ✓ 5214  ✗ 52 | 99.21% ✓ 6064 ✗ 48 | 98.86% ✓ 5148 ✗ 59 | - |
| - http_req_duration | - | - | 6.39s | 5.19s | 4.79s | 5.19s | 4.64s | 3.82s | **4.74s** | *5.0s* |
| - datasetSize:extraSmall | - | - | 6.59s | 5.41s | 5.54s | 5.41s | 3.82s | 3.67s | **4.28s** | *5.07s* |
| - datasetSize:small | - | - | 6.06s | 5.06s | 4.52s | 5.06s | 4.3s | 3.36s | **4.17s** | *4.72s* |
| - datasetSize:small_paginated | - | - | 6.13s | 4.27s | 3.82s | 4.27s | 3.69s | 3.04s | **3.61s** | *4.2s* |
| **Soak-test data extractions** | - | - | - | 100.00% ✓ 42590 ✗ 0 | 100.00% ✓ 46873 ✗ 0 | - | - | - | - | - |
| - http_req_duration | - | - | - | 1.34s | 1.13s | - | - | - | - | - |


# Quality Assurance

## Purpose
The purpose of this repository is to centralize the tools used for the quality-assurance of the components of the .stat-suite. 
- Currently the quality-assurance covers two areas:
	- **Performance testing** 
	- **Security testing**

### Performance testing
The performance tests have been configured in the GitLab CI pipelines, to automatically test the nsiws and transfer-services during the development process.
- These tests include performance testing for imports and extractons of data and structures.
> A detailed information about the performance tests can be found in the [README.md](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-quality-assurance/-/blob/master/PerformanceTests/README.md) in the PerformanceTests folder, in this repository.
- There are two pipelines configured to run two performance tests
	- **Triggered jobs** 
	- **Daily scheduled jobs**

### Security testing
The dynamic application/blackbox security tests (SAST) are run using [Netsparker](netsparkercloud.com). 
Only the Gitlab repositories containing public interfaces are scanned, which are listed in the [yaml](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-quality-assurance/-/blob/master/.netsparker-security-test-gitlab-ci.yml) pipeline description file.
Each .stat-suite Gitlab repository is scanned separatly and the result can be viewed in the Netsparker web GUI.
For more information on how to access scanned reports, please contact any Gitlab user withn the SIS-CC group that as a owner role.  

## CI pipeline setup

### Pipeline triggers
The Quality assurance CI pipeline gets triggered by either when a commit is done on the develop branch in one of the .Stat-suite component repositories or as a scheduled job. 
More details for the Performance tests is documented separatly [here](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-quality-assurance/-/blob/master/PerformanceTests/README.md). 

###  Performance tests
The triggered performance test jobs contain light performance tests for imports and exports of data and structures.
- The jobs are configured to be triggered by a merge of a feature branch into development in the [dotstatsuite-core-transfer repository](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer) 
- The objective of this tests is to:
	- Provide a sanity check every time there are new changes to the NSI-WS and the transfer-service.
	- Verify that these systems doesn't throw any errors when under minimal load.
- The file **.smoke-performance-test-gitlab-ci** describes the steps to be executed.
    - The tests are run against the "qa:reset" space of the [nsi-ws QA environment](http://nsi-reset-qa.siscc.org/) and the [transfer-service QA environment](http://transfer-qa.siscc.org/)
    - The summary of the result of each test run can be found as a downloadable json file in the [job artifacts](https://docs.gitlab.com/ee/ci/pipelines/job_artifacts.html).

###### Scheduled CI jobs 
The scheduled jobs contain extensive performance tests for imports and exports of data and structures. 
- They are configured to run every weekend to avoid affecting the users, by the high traffic caused by the performance tests.
- These tests can be launched manually, if needed, from the [list of scheduled pipelines](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-quality-assurance/-/pipeline_schedules)
- The objective of this tests is to:
	- Assess the current performance of the NSI-WS and the transfer-service under typical and peak load.
	- Make sure that these systems are continuously meeting the performance standards as changes are made to the system (code).
- The file **.extended-performance-test-gitlab-ci.yml** describes the steps to be executed.
    - The tests are run against the "qa:stable" space of the [nsi-ws QA environment](http://nsi-stable-qa.siscc.org/) and the [transfer-service QA environment](http://transfer-qa.siscc.org/)
    - The summary of the result of each test run can be found as a downloadable json file in the [job artifacts](https://docs.gitlab.com/ee/ci/pipelines/job_artifacts.html).




